FROM ubuntu:22.04 AS builder

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
  apt-get install -y build-essential make automake pkg-config libcurl4-openssl-dev libjson-c-dev

WORKDIR /root/ppastats
COPY . .

RUN autoreconf
RUN ./configure
RUN make

RUN find themes -name 'Makefile*' -exec rm {} \;


FROM ubuntu:22.04

ENV DEBIAN_FRONTEND=noninteractive

RUN \
  apt-get update && \
  apt-get install -y libcurl4 libjson-c5 && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/*

COPY --from=builder /root/ppastats/src/ppastats /usr/local/bin/ppastats
COPY --from=builder /root/ppastats/themes /usr/local/share/ppastats/themes

RUN mkdir -p /root/.ppastats

ENTRYPOINT [ "ppastats" ]
